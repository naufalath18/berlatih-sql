CREATE DATABASE myshop

USE myshop

MariaDB [myshop]> CREATE TABLE users ( id int NOT NULL AUTO_INCREMENT, name varchar(255), email varchar(255), password varchar(255), PRIMARY KEY (id));
Query OK, 0 rows affected (0.074 sec)

MariaDB [myshop]> describe users;
+----------+--------------+------+-----+---------+----------------+
| Field    | Type         | Null | Key | Default | Extra          |
+----------+--------------+------+-----+---------+----------------+
| id       | int(11)      | NO   | PRI | NULL    | auto_increment |
| name     | varchar(255) | YES  |     | NULL    |                |
| email    | varchar(255) | YES  |     | NULL    |                |
| password | varchar(255) | YES  |     | NULL    |                |
+----------+--------------+------+-----+---------+----------------+
4 rows in set (0.041 sec)

MariaDB [myshop]> CREATE TABLE categories ( id int NOT NULL AUTO_INCREMENT, name varchar(255), PRIMARY KEY (id));
Query OK, 0 rows affected (0.057 sec)

MariaDB [myshop]> describe categories;
+-------+--------------+------+-----+---------+----------------+
| Field | Type         | Null | Key | Default | Extra          |
+-------+--------------+------+-----+---------+----------------+
| id    | int(11)      | NO   | PRI | NULL    | auto_increment |
| name  | varchar(255) | YES  |     | NULL    |                |
+-------+--------------+------+-----+---------+----------------+
2 rows in set (0.045 sec)

MariaDB [myshop]> CREATE TABLE items ( id int NOT NULL AUTO_INCREMENT, name varchar(255), description varchar(255), price int, stock int, category_id int, PRIMARY KEY (id), CONSTRAINT FK_CategoryItem FOREIGN KEY(category_id) REFERENCES categories(id));
Query OK, 0 rows affected (0.059 sec)

MariaDB [myshop]> describe items;
+-------------+--------------+------+-----+---------+----------------+
| Field       | Type         | Null | Key | Default | Extra          |
+-------------+--------------+------+-----+---------+----------------+
| id          | int(11)      | NO   | PRI | NULL    | auto_increment |
| name        | varchar(255) | YES  |     | NULL    |                |
| description | varchar(255) | YES  |     | NULL    |                |
| price       | int(11)      | YES  |     | NULL    |                |
| stock       | int(11)      | YES  |     | NULL    |                |
| category_id | int(11)      | YES  | MUL | NULL    |                |
+-------------+--------------+------+-----+---------+----------------+
6 rows in set (0.048 sec)

MariaDB [myshop]>  INSERT INTO users ( name, email, password ) values ("John Doe", "john@doe.com", "john123"),("Jane Doe", "jane@doe.com", "jenita123");
Query OK, 2 rows affected (0.011 sec)
Records: 2  Duplicates: 0  Warnings: 0

MariaDB [myshop]> select * from users;
+----+----------+--------------+-----------+
| id | name     | email        | password  |
+----+----------+--------------+-----------+
|  1 | John Doe | john@doe.com | john123   |
|  2 | Jane Doe | jane@doe.com | jenita123 |
+----+----------+--------------+-----------+
2 rows in set (0.000 sec)

MariaDB [myshop]> INSERT INTO categories ( name )
    -> VALUES ( "gadget" );
Query OK, 1 row affected (0.010 sec)

MariaDB [myshop]> INSERT INTO categories ( name )
    -> VALUES ( "cloth" ),("men"),("women"),("branded");
Query OK, 4 rows affected (0.011 sec)
Records: 4  Duplicates: 0  Warnings: 0

MariaDB [myshop]> select * from categories;
+----+---------+
| id | name    |
+----+---------+
|  1 | gadget  |
|  2 | cloth   |
|  3 | men     |
|  4 | women   |
|  5 | branded |
+----+---------+
5 rows in set (0.000 sec)

MariaDB [myshop]> INSERT INTO items ( name, description, price, stock, category_id )
    -> values ("Sumsang b50","hape keren dari merek sumsang","4000000","100","1");
Query OK, 1 row affected (0.011 sec)

MariaDB [myshop]> INSERT INTO items ( name, description, price, stock, category_id )
    -> values ("Uniklooh","baju keren dari brand tername","500000","50","2");
Query OK, 1 row affected (0.002 sec)

MariaDB [myshop]> INSERT INTO items ( name, description, price, stock, category_id )
    -> values ("IMHO Watch","jam tangan anak yang jujur banget","2000000","10","1");
Query OK, 1 row affected (0.002 sec)

MariaDB [myshop]> select * from items;
+----+-------------+-----------------------------------+---------+-------+-------------+
| id | name        | description                       | price   | stock | category_id |
+----+-------------+-----------------------------------+---------+-------+-------------+
|  1 | Sumsang b50 | hape keren dari merek sumsang     | 4000000 |   100 |           1 |
|  2 | Uniklooh    | baju keren dari brand tername     |  500000 |    50 |           2 |
|  3 | IMHO Watch  | jam tangan anak yang jujur banget | 2000000 |    10 |           1 |
+----+-------------+-----------------------------------+---------+-------+-------------+
3 rows in set (0.000 sec)

MariaDB [myshop]> SELECT  id, name , email from users;
+----+----------+--------------+
| id | name     | email        |
+----+----------+--------------+
|  1 | John Doe | john@doe.com |
|  2 | Jane Doe | jane@doe.com |
+----+----------+--------------+
2 rows in set (0.001 sec)

MariaDB [myshop]> SELECT * FROM items
    -> WHERE price >= 1000000;
+----+-------------+-----------------------------------+---------+-------+-------------+
| id | name        | description                       | price   | stock | category_id |
+----+-------------+-----------------------------------+---------+-------+-------------+
|  1 | Sumsang b50 | hape keren dari merek sumsang     | 4000000 |   100 |           1 |
|  3 | IMHO Watch  | jam tangan anak yang jujur banget | 2000000 |    10 |           1 |
+----+-------------+-----------------------------------+---------+-------+-------------+
2 rows in set (0.000 sec)

MariaDB [myshop]> SELECT * FROM items
    -> WHERE name LIKE "%sang%";
+----+-------------+-------------------------------+---------+-------+-------------+
| id | name        | description                   | price   | stock | category_id |
+----+-------------+-------------------------------+---------+-------+-------------+
|  1 | Sumsang b50 | hape keren dari merek sumsang | 4000000 |   100 |           1 |
+----+-------------+-------------------------------+---------+-------+-------------+
1 row in set (0.000 sec)

MariaDB [myshop]> SELECT items.name, items.description, items.price, items.stock , items.category_id, categories.name
    -> from items
    -> inner join categories on items.category_id=categories.id
    -> ;
+-------------+-----------------------------------+---------+-------+-------------+--------+
| name        | description                       | price   | stock | category_id | name   |
+-------------+-----------------------------------+---------+-------+-------------+--------+
| Sumsang b50 | hape keren dari merek sumsang     | 4000000 |   100 |           1 | gadget |
| Uniklooh    | baju keren dari brand tername     |  500000 |    50 |           2 | cloth  |
| IMHO Watch  | jam tangan anak yang jujur banget | 2000000 |    10 |           1 | gadget |
+-------------+-----------------------------------+---------+-------+-------------+--------+
3 rows in set (0.001 sec)

MariaDB [myshop]> update items
    -> set price="2500000"
    -> where name="Sumsang b50";
Query OK, 1 row affected (0.005 sec)
Rows matched: 1  Changed: 1  Warnings: 0

MariaDB [myshop]> select * from items
    -> ;
+----+-------------+-----------------------------------+---------+-------+-------------+
| id | name        | description                       | price   | stock | category_id |
+----+-------------+-----------------------------------+---------+-------+-------------+
|  1 | Sumsang b50 | hape keren dari merek sumsang     | 2500000 |   100 |           1 |
|  2 | Uniklooh    | baju keren dari brand tername     |  500000 |    50 |           2 |
|  3 | IMHO Watch  | jam tangan anak yang jujur banget | 2000000 |    10 |           1 |
+----+-------------+-----------------------------------+---------+-------+-------------+
3 rows in set (0.000 sec)
